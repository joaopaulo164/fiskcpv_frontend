angular
    .module('appRoutes', ["ui.router"])
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $stateProvider.state({
        name: 'song',
        url: '/',
        templateUrl: 'public/components/song/templates/song.template.html',
        controller: 'SongController'
    });

    $urlRouterProvider.otherwise('/');
}]);
