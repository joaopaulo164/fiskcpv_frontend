'use strict';

var song = angular.module("song", []);

song.config(function($sceDelegateProvider) {
  $sceDelegateProvider.resourceUrlWhitelist([
      // Allow same origin resource loads.
      'self',
      'https://www.youtube.com/**'
  ]);

  // The blacklist overrides the whitelist so the open redirect here is blocked.
  $sceDelegateProvider.resourceUrlBlacklist([
    //'http://myapp.example.com/clickThru**'
  ]);

});

angular
    .module('SongApplication', [
        'appRoutes',
        'song',
        'ngResource'
    ]);
