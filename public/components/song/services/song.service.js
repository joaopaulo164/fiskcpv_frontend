song
    .factory('SongService', function($resource) {
        return $resource(
            //'http://127.0.0.1:8000/songs/?format=json',
            'https://fisk-project.herokuapp.com/songs/?format=json',
            //'http://192.168.1.40:8000/songs/?format=json',
            {},
            {
                'query': {
                    method: 'GET',
                    isArray: true,
                    headers: {
                        'Content-Type':'application/json'
                    }
                }
            },
            {
                stripTrailingSlashes: false
            }
        );
    });