song
    .controller('SongController', function ($scope, $q, $timeout, SongService, $interval) {

        $scope.currentVerseSentence = '';
        $scope.currentVerse = '';
        $scope.currentVerseDuration = 0;
        $scope.currentVerseMillisecondsDifference = 0;
        $scope.corretAnswerCount = 0;
        $scope.wrongAnswerCount = 0;

        $scope.songs = SongService.query();
        $scope.song = {};
        $scope.leftCount = 0;

        //$scope.running.app = startVerseTimeTracking($scope.songs);

        $scope.resolveAll = function (response) {
            // response is an array of both resolved promises
            console.log("RESOLVE ALL");
            console.log(response);
            return response
        };

        $q.all([$scope.songs.$promise]).then($scope.resolveAll).then(function () {
            startVerseTimeTracking($scope.songs);

        });


        var getSong = function (songID) {
            forEach($scope.songs, function (song) {
                if (song.id == songID) {
                    return song
                } else {
                    return null
                }

            })
        };

        var getDuration = function (song) {
            if (song != null) {
                return song.duration;
            } else {
                return null
            }
        };

        var getVerses = function (song) {
            if (song) {
                console.log(song);
                $scope.allVerses = song.verses;
                //return song.verses;
            }
        };

        var getCurrentVerse = function (orderVerse, song) {
            getVerses(song);
            var verses = $scope.allVerses;
            console.log('current verses');
            console.log(verses);
            verses.forEach(function (verse) {
                if (verse.order == orderVerse) {
                    console.log('passou');
                    console.log('o verso é');
                    console.log(verse);
                    $scope.currentVerse = verse;
                }
            })
        };

        var getNextVerse = function (currentVerseOrder, song) {
            console.log('currentVerseOrder');
            console.log(currentVerseOrder);
            //getVerses(song);
            var verses = $scope.allVerses;
            verses.forEach(function (verse) {
                console.log(verse.order);
                if (verse.order == currentVerseOrder + 1) {
                    console.log('passou getNextVerse');
                    console.log(verse);
                    $scope.currentVerse = verse;
                    return verse;
                }
            })
        };

        var hmsToSeconds = function (strHHmmss) {
            var p = strHHmmss.split(':'),
                s = 0, m = 1;

            while (p.length > 0) {
                s += m * parseInt(p.pop(), 10);
                m *= 60;
            }

            return s;
        };

        var updateVerse = function (song) {
            var order = $scope.currentVerse.order + 1;
            getNextVerse(order, $scope.currentVerse);
            var verse = getCurrentVerse(order, song);
            var verse = $scope.currentVerse;
            //console.log('start song function');
            //console.log(song);
            //console.log('start verse function');
            //console.log(verse);
            //console.log('start verse end function');
            //console.log(verse.end);

            var verseEnd = hmsToSeconds(verse.end);
            console.log("verseEnd: " + verseEnd);
            var verStart = hmsToSeconds(verse.start);
            console.log("verseStart: " + verStart);
            var verseEndMilliseconds = verse.endMilliseconds;
            var verStartMilliseconds = verse.startMilliseconds;
            var millisecondsDifference = verseEndMilliseconds - verStartMilliseconds;
            $scope.currentVerseMillisecondsDifference = millisecondsDifference;
            console.log("currentVerseMillisecondsDifference: " + $scope.currentVerseMillisecondsDifference);
            $scope.currentVerseDuration = verseEnd - verStart + 2;
            $scope.currentVerseSentence = verse.sentence;
        };

        function startVerseTimeTracking(songs) {
            console.log(songs);
            var song = songs[0];
            $scope.song = song;
            console.log('É a música');
            console.log(song);

            $scope.leftCount = song.verses.length;

            //var promise = $interval(function () {
            //    ++$scope.currentExerciseDuration;
            //    --$scope.workoutTimeRemaining;
            //}, 1000, $scope.currentExercise.duration - $scope.currentExerciseDuration);
            //
            //promise.then(function () {
            //    var next = getNextExercise($scope.currentExercise);
            //    if (next) {
            //        $scope.carousel.next();
            //        startExercise(next);
            //    }
            //    else {
            //        workoutComplete();
            //    }
            //}, function (error) {
            //    console.log('Inteval promise cancelled. Error reason -' + error);
            //});
            //return promise;
            var timePassed = 0;
            var timeRemainig = hmsToSeconds(song.duration);
            var order = 0;
            var verse = getCurrentVerse(order, song);
            var verse = $scope.currentVerse;
            console.log('start song function');
            console.log(song);
            console.log('start verse function');
            console.log(verse);
            console.log('start verse end function');
            console.log(verse.end);
            var verseEnd = hmsToSeconds(verse.end);
            console.log("verseEnd: " + verseEnd);
            var verStart = hmsToSeconds(verse.start);
            console.log("verseStart: " + verStart);
            var verseEndMilliseconds = verse.endMilliseconds;
            var verStartMilliseconds = verse.startMilliseconds;
            var millisecondsDifference = verseEndMilliseconds - verStartMilliseconds;
            $scope.currentVerseMillisecondsDifference = millisecondsDifference;
            console.log("currentVerseMillisecondsDifference: " + $scope.currentVerseMillisecondsDifference);
            $scope.currentVerseDuration = verseEnd - verStart + 1;
            $scope.currentVerseSentence = $scope.currentVerse.sentence;


            setTimeout(function () {
                //$scope.currentVerseSentence = $scope.currentVerse.sentence;
                $interval(function () {
                    //$scope.currentVerseSentence = $scope.currentVerse.sentence;
                    updateVerse(song);

                    //Resolving error: "digest already in progress"
                    $timeout(function () {
                        $scope.$apply();
                    }, 0);

                }, ($scope.currentVerseDuration * 1000) + $scope.currentVerseMillisecondsDifference, $scope.song.verses.length - 1)
                    .error(function (error) {
                        console.log('Inteval promise cancelled. Error reason -' + error);
                    });

            }, (((verStart + 3) * 1000) + verStartMilliseconds));


            //
            //setTimeout(function () {
            //
            //    var i = 0;
            //    var j = 100;
            //
            //    while(i < j){
            //        console.log("i");
            //        console.log(i);
            //        $scope.$apply(function () {
            //            updateVerse(song);
            //            getCurrentVerse($scope.currentVerse.order, song);
            //            i = i + 1;
            //     });
            //    }
            //
            //}, $scope.currentVerseDuration * 1000);
            //

            //var promise = $interval(function () {
            //    ++order;
            //    ++timePassed;
            //    --timeRemainig;
            //}, 1000, currentVerseDuration - timePassed);
            //
            //promise.then(function () {
            //    var next = getNextVerse(order, song);
            //    console.log('NEXT');
            //    console.log(next);
            //    if (next) {
            //        console.log('NEXT');
            //        console.log(next);
            //        $scope.carousel.next();
            //        //startExercise(next);
            //    }
            //    else {
            //        console.log('xabu');
            //        //workoutComplete();
            //    }
            //}, function (error) {
            //    console.log('Inteval promise cancelled. Error reason -' + error);
            //});
            //return promise

        };

        $scope.wordOptionsClick = function (answer, verse) {
            if (answer == verse.secret_word){
                //alert("Opção correta!!... =)");
                verse.correctAnswer = true;
                verse.statusAnswer = "Correta";
                verse.myAnswer = answer;
                verse.buttonDisable = true;
                $scope.corretAnswerCount = $scope.corretAnswerCount +1;
                $scope.leftCount = $scope.leftCount - 1;

            } else {
                //alert("Opção errada!!!... =(");
                verse.correctAnswer = false;
                verse.statusAnswer = "Errada";
                verse.myAnswer = answer;
                verse.buttonDisable = true;
                $scope.wrongAnswerCount = $scope.wrongAnswerCount +1;
                $scope.leftCount = $scope.leftCount - 1;
            }
        }

    });
